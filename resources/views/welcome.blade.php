

<!DOCTYPE html>
<html>
<head>
  <title>Informasi| BPBD Semarang</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="refresh" content="900">
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" media="screen" href="{{ asset('css/stylefix.css') }}">
  <link rel="shortcut icon" href="">
  <link rel="author" content="faiz">
  <style type="text/css">
  ::-webkit-scrollbar { 
    display: none;
    html, body {
  height: 100%;
}

#wrap {
  min-height: 100%;
}

#main {
  overflow:auto;
  padding-bottom:150px; /* this needs to be bigger than footer height*/
}

.footer {
  position: relative;
  margin-top: -150px; /* negative value of footer height */
  height: 150px;
  clear:both;
  padding-top:20px;
</style>
</head>
<body>
  <div class="wrapper" style="min-height: 100vh; max-height: 100vh; background-color: #ff5e00">
    <header id="judul">
      <div id="logo">
        <img src="{{ asset('image/header.png') }}" alt="Kota Semarang" class="logo-image">
      </div>
      <div id="sekarang" class="text-center">
        <div id="hari"></div>
        <div id="tanggal"></div>
        <div id="waktu"></div>  
      </div> <!-- /#sekarang -->
    </header>
    <br>
    <div style="min-height: 28vh">
      
    <table id="tkegiatan" class="table table-bordered" style="margin-bottom: 0" >
      <thead>
        <tr bgcolor="#730701">
          <th style="text-align: center;" colspan="6">Jadwal Kegiatan</th>
        </tr>
        <tr bgcolor="#730701">
          <th>No</th>
          <th>Kejadian</th>
          <th>Tanggal</th>
          <th>Waktu</th>
          <th>Lokasi</th>
          <th>Venue</th>
        </tr>
      </thead>
      
      @if($kegiatan > 1)
      <tbody id="kegiatan" >
        @else
        <tbody>
          @endif
          @foreach($data1 as $index=>$a)
          @if(($index % 2) == 1)
          <tr bgcolor="#b5170b">
            @else
            <tr bgcolor="#ff6d29">
              @endif
              <td>{{$index+1}}</td> 
              <td>{{$a->nama}}</td>
              <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $a->tanggal)->format('d/m/Y')}}</td> 
              <td>{{$a->jam}}</td>
              <td>{{$a->nama_kelurahan}},{{$a->nama_kecamatan}}</td>
              <td>{{$a->lokasi}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
    <div style="min-height: 50vh">
        <table class="table table-bordered" style="margin-bottom: 0">
          <thead>
            <tr bgcolor="#730701">
              <th style="text-align: center;" colspan="6">Informasi Bencana</th>
            </tr>
            <tr bgcolor="#730701">
              <th>No</th>
              <th>Kejadian</th>
              <th>Tanggal</th>
              <th>Waktu</th>
              <th>Lokasi</th>
              <th>Gambar</th>
            </tr>
          </thead>
          @if($kejadian > 2)
          <tbody id="kejadian" >
            @else
            <tbody>
              @endif
              @foreach($data as $index=>$a)
              @if(($index % 2) == 1)
              <tr bgcolor="#b5170b">
                @else
                <tr bgcolor="#ff6d29">
                  @endif
                  <td>{{$index+1}}</td> 
                  <td>{{$a->nama}}</td>
                  <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $a->tanggal)->format('d/m/Y')}}</td> 
                  <td>{{$a->jam}}</td>
                  <td>{{$a->nama_kelurahan}},{{$a->nama_kecamatan}}</td>
                  <td> <img src="{{ asset('gambar/'.$a->gambar) }}" height="200px" width="300px"></td>
                </tr>
                @endforeach
              </tbody>
            </table>
         </div>
            <footer class="">

              <div id="running">
                <marquee behavior='scroll' scrollamount="2" direction="left">
                  <p>
                  Papan Informasi Badan Penanggulanan Bencana Kota Semarang || Kompleks Terminal Penggaron, Jl. Brigjen Sudiarto No.KM. 11, Penggaron Kidul, Kec. Pedurungan, Kota Semarang, Jawa Tengah 50194</p>
                </marquee>
              </div> <!-- /#running -->
            </footer>
          </div>


          <script type='text/javascript' src="{{ asset('js/jquery.js') }}"></script>
          <script type='text/javascript' src="{{ asset('js/carousel.js') }}"></script>
          <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
          <script type='text/javascript' src="{{asset('js/weather.js')}}"></script>
          <script>
            $(document).ready(function() {  
      getWeather(); //Get the initial weather.
      setInterval(getWeather, 600000); //Update the weather every 10 minutes.
    });
            function getWeather() {
              $.simpleWeather({
                location: 'Semarang, ID',
                woeid: '',
                unit: 'c',
                success: function(weather) {
                  html = '<p>'+'<i class="icon-'+weather.code+'"></i>'+weather.temp+'&deg;'+weather.units.temp+'</p>';

                  $("#weather").html(html);
                },
                error: function(error) {
                  $("#weather").html('<p>'+error+'</p>');
                }
              });
            };
          </script>
          <script type="text/javascript">
            setInterval(function() {
              $.ajax({
               type:'GET',
               url:'/check_kegiatan',
               success:function(j){
                if(<?php echo $kegiatan ?> != j){
                  location.reload();
                }
              }
            });
            $.ajax({
               type:'GET',
               url:'/check_kegiatan1',
               success:function(i){
                if(JSON.stringify(<?php echo $datas1 ?>) == i){
                }else{
                  location.reload();
                }
              }
            });
            },10000)
          </script>
          <script type="text/javascript">
            setInterval(function() {
              $.ajax({
               type:'GET',
               url:'/check_kejadian',
               success:function(x){
                if(<?php echo $kejadian ?> != x){
                  location.reload();
                }
              }
            });
              $.ajax({
               type:'GET',
               url:'/check_kejadian1',
               success:function(y){
                if(JSON.stringify(<?php echo $datas ?>) == y){
                }else{
                  location.reload();
                }
              }
            });
            },10000)
          </script>
          
<script type="text/javascript">
  $.fn.infiniteScrollUp=function(){
    var self=this,kids=self.children()
    kids.slice(5).hide()
    setInterval(function(){
      kids.eq(0).fadeOut(function(){
        $(this).appendTo(self)
        kids=self.children()
      kids.filter(':hidden').eq(0).fadeIn()
      })
    },8000)
    return this
  }
  $(function(){
    $('#kegiatan').infiniteScrollUp()
  })
</script>

<script type="text/javascript">
  $.fn.infiniteScrollUp1=function(){
    var self=this,kids=self.children()
    kids.slice(2).hide()
    setInterval(function(){
      kids.eq(0).fadeOut(function(){
        $(this).appendTo(self)
        kids=self.children()
      kids.filter(':hidden').eq(0).fadeIn()
      })
    },10000)
    return this
  }
  $(function(){
    $('#kejadian').infiniteScrollUp1()
  })
</script>
<script type='text/javascript'>

  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth();
  var thisDay = date.getDay(),
  thisDay = myDays[thisDay];
  var yy = date.getYear();
  var year = (yy < 1000) ? yy + 1900 : yy;
  document.getElementById('hari').innerHTML=thisDay + ", ";
  document.getElementById('tanggal').innerHTML=day + ' ' + months[month] + ' ' + year;
          //-->
        </script>
        <script type="text/javascript">
          <!-- // Waktu
          function startTime() {
            var today=new Date(),
            curr_hour=today.getHours(),
            curr_min=today.getMinutes(),
            curr_sec=today.getSeconds();
            curr_hour=checkTime(curr_hour);
            curr_min=checkTime(curr_min);
            curr_sec=checkTime(curr_sec);
            document.getElementById('waktu').innerHTML=curr_hour+":"+curr_min+":"+curr_sec+"  "+"<span class=\"wilayah\">wib</span>";
          }
          function checkTime(i) {
            if (i<10) {
              i="0" + i;
            }
            return i;
          }
          setInterval(startTime, 500);
          //-->
        </script>
      </body>
      </html>