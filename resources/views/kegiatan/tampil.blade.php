@extends('layouts.layoutAdmin')  

@section('content')


<section class="content-header">
      <h1>
        Data Kegiatan
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <!-- /.box -->

          <div class="box">
          <div class="box-header">
          <a href="tambah_kegiatan" class="btn btn-primary">Tambah</a>
          </div>
          <div class="box-body">
            
            <!-- /.box-header -->
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Tanggal</th>
                  <th>Waktu</th>
                  <th>Lokasi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php $count = 1; ?>
                  @foreach($data as $data)
                  <tr>
                    <td> {{ $count }} </td>
                    <td> {{ $data->nama}} </td>
                    <td> {{ $data->tanggal}} </td>
                    <td> {{ $data->jam}} </td>
                    <td> {{ $data->lokasi}} </td>
                    <td>
                       <a href="edit_kegiatan/{{ $data->id}}" class="btn btn-success">Edit</a>
                      <a href="hapus_kegiatan/{{$data->id }}" onclick="return confirm ('Apakah Anda Yakin Menghapus Data ini ?'); " class="btn btn-danger"> Hapus </a>
                    </td>
                  </tr>
                  <?php $count++; ?>
                  @endforeach 

               
               
               
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection