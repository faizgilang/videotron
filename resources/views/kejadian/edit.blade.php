@extends('layouts.layoutAdmin')  

@section('content')
  <!-- Horizontal Form -->
          <section class="content">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Tambah Kejadian</h3>
              @if(session()->has('message'))
              <div class="">
              Alert::message('Message', 'Optional Title');
              {{ session()->get('message') }}
             </div>
            @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="{{ url()->current() }}"class="form-horizontal" enctype="multipart/form-data">

              {{ csrf_field() }}

              
              <div class="box-body">
               
                <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Masukkan Nama Kejadian" name="nama" value="{{ $kejadian->nama }}">
                    {!! $errors->first('nama', '<strong class="text-danger">:message</strong>') !!}
                  </div>
                </div>
               <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Tanggal</label>

                  <div class="col-sm-10">
                    <input type="date" class="form-control" placeholder="Masukkan tanggal kejadian" name="tanggal" value="{{ $kejadian->tanggal }}">
                    {!! $errors->first('tanggal', '<strong class="text-danger">:message</strong>') !!}
                  </div>
                </div>
               <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Waktu</label>

                  <div class="col-sm-10">
                    <input type="time" class="form-control" placeholder="Masukkan waktu kejadian" name="jam" value="{{ $kejadian->jam}}">
                    {!! $errors->first('jam', '<strong class="text-danger">:message</strong>') !!}
                  </div>
                </div>
            
                <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Kecamatan</label>
                 


                  <div class="col-sm-10">
                    <select class="form-control" name="kecamatan">
                    <option value="">Kecamatan</option>
                    @foreach($kecamatan as $kecamatan)
                      <option value="{{$kecamatan->id_kecamatan}}">{{$kecamatan->nama_kecamatan}}</option>
                    @endforeach
                    </select>
                     {!! $errors->first('kecamatan', '<strong class="text-danger">:message</strong>') !!}

                  </div>
                </div>
                
                <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Kelurahan</label>
                 


                  <div class="col-sm-10">
                    <select class="form-control" name="lokasi">
                    <option value="">Kelurahan</option>
                    
                    </select>
                     {!! $errors->first('lokasi', '<strong class="text-danger">:message</strong>') !!}

                  </div>
                </div>
                <div class="form-group">
                    <label for="text" class="col-sm-2 control-label">Gambar</label>
                  <div class="col-sm-10">
                    <input type="file" name="file">
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </section>

@endsection
@section('script')
          <script type="text/javascript">
          jQuery(document).ready(function ()
       {
            jQuery('select[name="kecamatan"]').on('change',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/kejadian/kelurahan/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="lokasi"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="lokasi"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="lokasi"]').empty();
               }
            });
    });
    </script>
@endsection
