
<link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" media="screen" href="{{ asset('css/stylefix.css') }}">
    
    <table class="table table-bordered" style="margin-bottom: 0">
      <thead>
        <tr bgcolor="#730701">
          <th colspan="5">Jadwal Kegiatan</th>
        </tr>
        <tr bgcolor="#730701">
          <th>No</th>
          <th>Kejadian</th>
          <th>Tanggal</th>
          <th>Waktu</th>
          <th>Lokasi</th>
        </tr>
      </thead>
      
      <tbody id="kegiatan" >
        @foreach($data1 as $index=>$a)
        @if(($index % 2) == 1)
        <tr bgcolor="#b5170b">
          @else
          <tr bgcolor="#ff6d29">
            @endif
            <td>{{$index+1}}</td> 
            <td>{{$a->nama}}</td>
            <td>{{$a->tanggal}}</td> 
            <td>{{$a->jam}}</td>
            <td> aaa</td>
          </tr>
          @endforeach
        </tbody>
      </table>



      <script type='text/javascript' src="{{ asset('js/jquery.js') }}"></script>
      <script type='text/javascript' src="{{ asset('js/carousel.js') }}"></script>
      <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>  <script type='text/javascript' src="{{ asset('js/carousel.js') }}"></script>
  <script type="text/javascript">
        $.fn.infiniteScrollUp=function(){
          var self=this,kids=self.children()
          kids.slice(4).hide()
          setInterval(function(){
            kids.filter(':hidden').eq(0).fadeIn()
            kids.eq(0).fadeOut(function(){
              $(this).appendTo(self)
              kids=self.children()
            })
          },5000)
          return this
        }
        $(function(){
          $('#kegiatan').infiniteScrollUp()
        })
  </script>