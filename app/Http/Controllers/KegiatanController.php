<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use DB;
use App\Http\Requests\KegiatanEdit;
use App\Http\Requests\KegiatanAdd;
use File;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
 public function __construct()
    {
        $this->middleware('auth');
     
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= Kegiatan::join('kelurahan','kelurahan.id_kelurahan','=','kegiatans.kelurahan')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->get();;
        return view ('kegiatan.tampil')->with('data',$data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kecamatan = DB::table('kecamatan')->get();
        return view ('kegiatan.tambah')
        ->with('kecamatan', $kecamatan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KegiatanAdd $request)
    {
        $detail = new Kegiatan;
        $detail->nama=$request->nama;
        $detail->jam=$request->jam;
        $detail->tanggal=$request->tanggal;
        $detail->lokasi=$request->lokasi;
        $detail->kelurahan=$request->kelurahan;
        $detail->save();

        return redirect('/tampil_kegiatan');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Kegiatan::find($id);
        $kecamatan = DB::table('kecamatan')->get();
        return view ('kegiatan.edit')
        ->with('kegiatan', $data)
        ->with('kecamatan', $kecamatan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(KegiatanEdit $request,$id)
    {
        //
           $detail = Kegiatan::find($id);
        $detail->nama=$request->nama;
        $detail->jam=$request->jam;
        $detail->tanggal=$request->tanggal;
        $detail->lokasi=$request->lokasi;
        $detail->kelurahan=$request->kelurahan;
        $detail->save();

        return redirect('/tampil_kegiatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
                $detail = Kegiatan::find($id);
        $detail->delete();
        return redirect('tampil_kegiatan');
    }
}
