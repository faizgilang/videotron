<?php

namespace App\Http\Controllers;
use App\Kejadian;
use App\Kegiatan;
use App\User;
use App\Transaksi;
use App\TransaksiToko;
use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function home()
    {
        
        $data= Kejadian::join('kelurahan','kelurahan.id_kelurahan','=','kejadians.lokasi')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kejadians.tanggal', '>=',Carbon::now()->subDays(7))
        ->get();
        $datas= Kejadian::join('kelurahan','kelurahan.id_kelurahan','=','kejadians.lokasi')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kejadians.tanggal', '>=',Carbon::now()->subDays(7))
        ->get()->toJson();
        
        $data1= Kegiatan::join('kelurahan','kelurahan.id_kelurahan','=','kegiatans.kelurahan')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kegiatans.tanggal', '>=',Carbon::now())
        ->get();
        $datas1= Kegiatan::join('kelurahan','kelurahan.id_kelurahan','=','kegiatans.kelurahan')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kegiatans.tanggal', '>=',Carbon::now())
        ->get()->toJson();

        $kejadian= Kejadian::count();
        $kegiatan= Kegiatan::count();
        return view('welcome')
        ->with('kegiatan',$kegiatan)
        ->with('kejadian',$kejadian)
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('datas',$datas)
        ->with('datas1',$datas1);
    }
    public function check_kejadian()
    {
        $check = Kejadian::count();


        return $check;
    }

    public function check_kegiatan()
    {
        $check = Kegiatan::count();
        return $check;
    }
      public function check_kejadian1()
    {
        $check= Kejadian::join('kelurahan','kelurahan.id_kelurahan','=','kejadians.lokasi')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kejadians.tanggal', '>=',Carbon::now()->subDays(7))
        ->get()->toJson();


        return $check;
    }

    public function check_kegiatan1()
    {
        $check = Kegiatan::join('kelurahan','kelurahan.id_kelurahan','=','kegiatans.kelurahan')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->whereDate('kegiatans.tanggal', '>=',Carbon::now())
        ->get()->toJson();
        return $check;
    }
    //    public function Kejadian()
    // {
    //     $data=Kejadian::all();
    //     return view ('kejadian.kejadian')->with('data',$data);
    // }
    // public function kegiatan()
    // {
    //     $data=Kegiatan::all();
    //     return view ('kegiatan.kegiatan')->with('data',$data);
    // }

    public function showDBAdmin()
    {

        $x = User::where('role','=','petugasbumdes')->count();
        $y = User::where('role','=','petugastoko')->count();

        return view('homeAdmin')
        ->with('user', $x)
        ->with('user', $y);
    }
       public function showDBDosen()
    {
        dd('dosen berhasil login,tampil dashboard');
    }
	
    public function showDBMahasiswa()
    {
        dd('mahasiswa berhasil login,tampil dashboard');
    }
}
