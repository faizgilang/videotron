<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserEdit;
use App\Http\Requests\UserAdd;
use App\User;
use Alert;
use Auth;

class UserController extends Controller
{   
    //Fungsi memanggil form tambah petugas
    public function addPetugas()
    {
    	return view ('user.tambah');
    }
    //Fungsi menyimpan tambah petuas
    public function savePetugas(UserAdd $request)
    {
       


        $user= new User;
        
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt ($request->password);

        $user->save();
                return redirect('/tampil_petugas');

    }
    //Fungsi untuk menampilkan
    public function index()
    {
    	$data=User::where('role','!=','admin')->get();
    	return view ('user.tampil')->with('user',$data);
    }

    public function editPetugas($id)
    {
        $data= User::find($id);
        return view ('user.edit')->with('user',$data);
    }

    
    public function updatePetugas (UserEdit $request,$id)
    {

        $user=User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        
        

        $user->save();
        Alert::success('Petugas Bumdes berhasil diubah', 'Sukses');
        return redirect('/tampil_petugas');

    }

    public function deletePetugas($id)
    {
        $user= User::findOrFail($id);
        
     
        $check= Transaksi::where('id_petugasBumdes','=',$id)->count();
        if ($check>0){
            Alert::warning('Petugas Bumdes tidak dapat dihapus karena petugas sudah memiliki data transaksi', 'Sorry');
        }
        else {
            $user -> delete();
            Alert::success('Petugas Bumdes berhasil dihapus', 'Sukses');
        }
        return redirect('/tampil_petugas');
    }
  
   
    public function resetPassword($id)
    {
        $data =User::find($id);
        $data->password=bcrypt('passwordbaru');
        $data->save();
        if($data->role == 'petugasbumdes'){
            Alert::success('Password telah berhasil direset, Password : passwordbaru', 'Sukses');
             return redirect('/tampil_petugas');
        } else {
            Alert::success('Password telah berhasil direset, Password : passwordbaru', 'Sukses');
             return redirect('/tampil_petugasToko');
         }
    }
   

}
