<?php

namespace App\Http\Controllers;

use App\Kejadian;
use Illuminate\Http\Request;
use App\Http\Requests\KejadianEdit;
use App\Http\Requests\KejadianAdd;
use DB;
use File;
use Storage;
use Illuminate\Support\Facades\Input;

class KejadianController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
     
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= Kejadian::join('kelurahan','kelurahan.id_kelurahan','=','kejadians.lokasi')
        ->join('kecamatan','kecamatan.id_kecamatan','=','kelurahan.id_kecamatan')
        ->get();
        return view ('kejadian.tampil')->with('data',$data);
    }

     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kecamatan = DB::table('kecamatan')->get();
        return view ('kejadian.tambah')
        ->with('kecamatan', $kecamatan);
    }

    public function get_kelurahan($id)
    {
        //
         $kelurahan = DB::table('kelurahan')
         ->where('id_kecamatan','=',$id)
         ->pluck('nama_kelurahan','id_kelurahan');

        return json_encode($kelurahan);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KejadianAdd $request)
    {
        $detail = new Kejadian;
        $detail->nama=$request->nama;
        $detail->jam=$request->jam;
        $detail->tanggal=$request->tanggal;
        $detail->lokasi=$request->lokasi;
        $detail->gambar="a";
        $detail->save();


        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Kejadian-' . $detail->id . '.' . $ext;
        $request->file('file')->move('gambar/', $filename);

        $data1=Kejadian::find($detail->id);
        $data1->gambar = $filename;
        $data1->save();

        return redirect('/tampil_kejadian');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kejadian  $kejadian
     * @return \Illuminate\Http\Response
     */
    public function show(Kejadian $kejadian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kejadian  $kejadian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Kejadian::find($id);
        $kecamatan = DB::table('kecamatan')->get();
        return view ('kejadian.edit')
        ->with('kejadian', $data)
        ->with('kecamatan', $kecamatan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kejadian  $kejadian
     * @return \Illuminate\Http\Response
     */
    public function update(KejadianEdit $request, $id)
    {
        //
        $detail = Kejadian::find($id);
        $detail->nama=$request->nama;
        $detail->jam=$request->jam;
        $detail->tanggal=$request->tanggal;
        $detail->lokasi=$request->lokasi;

        if ($request->file('file')) {
          File::delete('gambar/' . $detail->gambar);

          $ext = Input::file('file')->getClientOriginalExtension();
          $filename = 'Kejadian-' . $detail->id . '.' . $ext;
          $request->file('file')->move('gambar/', $filename);
        } 
        
        $detail->save();
        return redirect('tampil_kejadian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kejadian  $kejadian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $detail = Kejadian::find($id);
        $detail->delete();
        return redirect('tampil_kejadian');
    }
}
