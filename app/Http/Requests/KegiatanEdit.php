<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KegiatanEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'tanggal' => 'required',
            'jam' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'lokasi' => 'required',
        ];
    }
    public function messages()
    {
        return[
            'nama.required' => 'Nama tidak boleh kosong',
            'tanggal.required' => 'Tanggal tidak boleh kosong',
            'jam.required' => 'Jam tidak boleh kosong',
            'kecamatan.required' => 'kecamatan tidak boleh kosong',
            'kelurahan.required' => 'kelurahan tidak boleh kosong',
            'lokasi.required' => 'Kelurahan tidak boleh kosong',
           
    ];
    }
}
